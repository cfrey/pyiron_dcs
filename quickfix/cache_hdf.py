from ..modpatch import patch

import pyiron_base.storage.hdfio
import pyiron_atomistics.sphinx.base

del pyiron_base.storage.hdfio._open_hdf

class Use_cached_handle_context:
    def __init__(self,key):
        global open_file_cache
        self.fh = open_file_cache[key].filehandle
    def __enter__(self):
        return self.fh 
    def __exit__(self,type,value,traceback):
        pass

class Create_cached_handle_context:
    class CacheEntry:
        def __init__(self,hdfio_context):
            self.hdfio_context = hdfio_context
            self.filehandle=None
            
    def __init__(self,key,filename,mode,swmr):
        import h5io_browser.base
        global count_open_calls, open_file_cache
        count_open_calls[0] += 1
        open_file_cache[key] = self.CacheEntry(h5io_browser.base._open_hdf(filename,mode,swmr))
        self.key = key

    def __enter__(self):
        global open_file_cache
        fh = open_file_cache[self.key].hdfio_context.__enter__ ()
        open_file_cache[self.key].filehandle = fh
        return fh

    def __exit__(self,type,value,traceback):
        global open_file_cache
        context = open_file_cache[self.key].hdfio_context
        del open_file_cache[self.key]
        return context.__exit__(type,value,traceback)
        
@patch(pyiron_base.storage.hdfio)
def _open_hdf(filename,mode="r",swmr=False):
    global open_file_cache
    key = f"{mode}:{filename}"
    global count_open_calls
    count_open_calls[1] += 1

    if mode == "r" and f"r+:{filename}" in open_file_cache:
        key = f"r+:{filename}"
    if key in open_file_cache:
        fh = Use_cached_handle_context(key)
        # switch to swmr if requested (but never switch off)
        if swmr:
            fh.swmr = True
        return fh
    return Create_cached_handle_context(key,filename,mode,swmr)

        

setattr(pyiron_base.storage.hdfio,"Use_cached_handle_context",Use_cached_handle_context)
setattr(pyiron_base.storage.hdfio,"Create_cached_handle_context",Create_cached_handle_context)

pyiron_base.storage.hdfio.count_open_calls=[0,0]
pyiron_base.storage.hdfio.open_file_cache=dict ()
open_file_cache = pyiron_base.storage.hdfio.open_file_cache
count_open_calls=pyiron_base.storage.hdfio.count_open_calls

@patch(pyiron_base.storage.hdfio.FileHDFio)
def _list_all(self):
    """
    List all groups and nodes of the HDF5 file - where groups are equivalent to directories and nodes to files.

    Returns:
        dict: {'groups': [list of groups], 'nodes': [list of nodes]}
    """
    if self.file_exists:
        with _open_hdf(self.file_name) as hdf:
            groups, nodes = _list_groups_and_nodes(hdf=hdf, h5_path=self.h5_path)
            iopy_nodes = self._filter_io_objects(set(groups))
        return {
            "groups": sorted(list(set(groups) - iopy_nodes)),
            "nodes": sorted(list((set(nodes) - set(groups)).union(iopy_nodes))),
        }
    else:
        return {"groups": [], "nodes": []}

@patch(pyiron_base.storage.hdfio)
def _to_object(hdf, class_name=None, **kwargs):
    """
    Load the full pyiron object from an HDF5 file

    Args:
        class_name(str, optional): if the 'TYPE' node is not available in
                    the HDF5 file a manual object type can be set,
                    must be as reported by `str(type(obj))`
        **kwargs: optional parameters optional parameters to override init
                  parameters

    Returns:
        pyiron object of the given class_name
    """
    if "TYPE" not in hdf.list_nodes() and class_name is None:
        raise ValueError("Objects can be only recovered from hdf5 if TYPE is given")
    elif class_name is not None and class_name != hdf.get("TYPE"):
        raise ValueError(
            "Object type in hdf5-file must be identical to input parameter"
        )
    type_field = class_name or hdf.get("TYPE")
    module_path, class_name = _extract_module_class_name(type_field)
    class_object = _import_class(module_path, class_name)

    # Backwards compatibility since the format of TYPE changed
    if type_field != str(class_object):
        hdf["TYPE"] = str(class_object)

    if hasattr(class_object, "from_hdf_args"):
        init_args = class_object.from_hdf_args(hdf)
    else:
        init_args = {}

    init_args.update(kwargs)

    obj = class_object(**init_args)
    with _open_hdf(hdf.file_name):
        obj.from_hdf(hdf=hdf.open(".."), group_name=hdf.h5_path.split("/")[-1])
    if static_isinstance(obj=obj, obj_type="pyiron_base.jobs.job.generic.GenericJob"):
        module_name = module_path.split(".")[0]
        module = importlib.import_module(module_name)
        if hasattr(module, "Project"):
            obj.project_hdf5._project = getattr(module, "Project")(
                obj.project_hdf5.project.path
            )
    return obj

@patch(pyiron_atomistics.sphinx.base.SphinxBase)
def from_hdf(self, hdf=None, group_name=None):
    """
    Recreates instance from the hdf5 file

    Args:
        hdf (str): Path to the hdf5 file
        group_name (str): Name of the group which contains the object
    """
    import pyiron_base.storage.hdfio
    with pyiron_base.storage.hdfio._open_hdf(self._hdf5.file_name):
        if "HDF_VERSION" not in self._hdf5.keys():
            from pyiron_base import GenericParameters
    
            super(SphinxBase, self).from_hdf(hdf=hdf, group_name=group_name)
            self._structure_from_hdf()
            gp = GenericParameters(table_name="input")
            gp.from_hdf(self._hdf5)
            for k in gp.keys():
                self.input[k] = gp[k]
        elif self._hdf5["HDF_VERSION"] == "0.1.0":
            super(SphinxBase, self).from_hdf(hdf=hdf, group_name=group_name)
            self._structure_from_hdf()
            with self._hdf5.open("input") as hdf:
                self.input.from_hdf(hdf, group_name="parameters")
        self.output.from_hdf(self._hdf5)

@patch(pyiron_atomistics.sphinx.base.SphinxBase)
def to_hdf(self, hdf=None, group_name=None):
    """
    Stores the instance attributes into the hdf5 file

    Args:
        hdf (str): Path to the hdf5 file
        group_name (str): Name of the group which contains the object
    """
    super(SphinxBase, self).to_hdf(hdf=hdf, group_name=group_name)
    import pyiron_base.storage.hdfio
    with pyiron_base.storage.hdfio._open_hdf(self._hdf5.file_name,"r+",True):
        self._structure_to_hdf()
        with self._hdf5.open("input") as hdf:
            self.input.to_hdf(hdf)
        self.output.to_hdf(self._hdf5)

