from pyiron_dcs.modpatch import patch as mpatch
import pyiron_atomistics.sphinx.structure

@mpatch(pyiron_atomistics.sphinx.structure)
def read_atoms(filename):
    import re
    from types import GeneratorType
    import numpy as np

    class trigger_parser_base:
        def __init__ (self,filename):
            self.filename = filename
            self.filehandle = open(filename)
            self.line = self.filehandle.__iter__ ()
            self.triggers = []
            self.lineview = ''
            self.lineno = 0
        def parse(self):
            while True:
                #print ('Scanning: ' + self.lineview)
                #print (self.triggers)
                for trig in self.triggers:
                    for key,func in trig.items ():
                        if key in self.lineview:
                            self._cleanup(trig)
                            #print ('Found key ' + key)
                            res = func ()
                            if isinstance(res,GeneratorType):
                                res.send (None)
                                trig['%finalize!'] = res
                            break
                    else:
                        continue
                    break
                else:
                    try:
                        self.lineview = next(self.line)
                        self.lineno += 1
                        self.line_from = self.lineno
                    except StopIteration:
                        break
                #print ('--')
            self._cleanup(self.triggers[0])
            try:
                self.finalize ()
            except AttributeError:
                pass
        def location(self):
            return f"in file '{self.filename}' line" + \
                   ( f" {self.lineno}" if self.lineno == self.line_from else
                     f"s {self.line_from}..{self.lineno}" )

        def read_until(self, match):
            while not match in self.lineview:
                self.lineview += next(self.line)
                self.lineno += 1
                self.lineFrom = self.line
            #print (self.lineview)

        def extract_via_regex(self, regex):
            if isinstance(regex, str):
                regex = re.compile (regex, re.DOTALL)
            result = regex.search(self.lineview)
            if result is None:
                raise RuntimeError(f"Failed to extract '{regex.pattern}' "
                                   + self.location ()
                                   + "\n" + self.lineview)
            self.lineview = regex.sub ('',self.lineview, count=1)
            return result.group ()

        def _cleanup(self, active):
            while (self.triggers[-1] is not active):
                try:
                    next(self.triggers[-1]['%finalize!'])
                except (KeyError,StopIteration):
                    pass
                del self.triggers[-1]
            try:
                next(active['%finalize!'])
                del active['%finalize!']
            except (KeyError, StopIteration):
                pass

    class struct_parser(trigger_parser_base):
        def __init__ (self, file):
            super().__init__(file)
            self.configs = []
            self.triggers.append ({'structure': self.parse_structure})
        def parse_structure(self):
            self.triggers.append ({
                'cell': self.parse_cell,
                'species' : self.parse_species})
            self.extract_via_regex('structure')
            self.cell = np.zeros((3,3))
            self.positions = []
            self.species = []
            self.indices = []
            self.ispecies=-1
            yield
            pse = PeriodicTable()
            atoms = Atoms(
                species=[pse.element (s) for s in self.species],
                indices=self.indices,
                cell=self.cell * BOHR_TO_ANGSTROM,
                positions=np.array(self.positions) * BOHR_TO_ANGSTROM,
                pbc=True
            )
            self.configs.append (atoms)

        def get_vector (self, key, txt):
            vecstring = re.sub ('.*' + key + r"\s*=\s*([^;]+);.*", r"\1", txt)
            if vecstring is None:
                raise RuntimeError(f"Cannot parse {key} from '{txt}' as vector "
                                   + self.location ())
            vecstring = re.sub(r"[][=,;$]",' ',vecstring)
            return np.fromstring(vecstring, sep=' ')

        def get_var(self, key, startend='=;'):
            self.read_until (startend[1])
            return self.extract_via_regex (key + r"\s*" + startend[0]
                                           + r"\s*[^" + startend[1] + ']+'
                                           + startend[1])

        def parse_cell (self):
            txt = self.get_var('cell')
            self.cell = self.get_vector('cell', txt).reshape (3,3)

        def parse_species (self):
            self.extract_via_regex('species')
            self.triggers.append ({
                'element' : self.get_element,
                'atom' : self.read_atom})
            self.ispecies += 1

        def get_element (self):
            txt=self.get_var ('element')
            self.species.append (re.sub ('.*"([^"]*)".*',r"\1",txt))

        def read_atom(self):
            txt=self.get_var ('atom', '{}')
            self.positions.append (self.get_vector('coords',txt))
            self.indices.append (self.ispecies)
            if 'label' in txt:
                label = re.sub (r'.*label\s*=\s*"([^"]+)"\s*;.*', r"\1", txt)
                print (f"atom {len(self.positions)} label={label}")

    p = struct_parser(filename)
    p.parse ()
    if len(p.configs) == 1:
        return p.configs[0]
    return p.configs

