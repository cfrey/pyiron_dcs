import functools
import types
import warnings
import copy
import sys

def restore(self):
    if getattr(self.__host__,self.__name__) is self:
        setattr(self.__host__,self.__name__,self.__orig_func__)
        del self.__host__
        del self.__orig_func__
        del self._restore_orig

def patch(host):
    """This decorator allows to patch a function in an existing class/module"""
    def install_patch(func):
        if hasattr(func,"__host__"):
            raise RuntimeError (f"Function {func.__name__} is already installed as a patch in {func.__host__}")
        if hasattr(host,func.__name__):
            hostglob = getattr(host,func.__name__).__globals__
        elif isinstance(host,types.ModuleType):
            hostglob = host.__dict__
        elif hasattr(host,'__init__'):
            hostglob = host.__init__.__globals__
        else:
            raise RuntimeError (f"Cannot identify namespace of host {host}")
        func2 = types.FunctionType(func.__code__, hostglob, name=func.__name__,
                           argdefs=func.__defaults__, closure=func.__closure__)
        func2.__kwdefaults__ = copy.copy(func.__kwdefaults__)
        func2.__doc__ = func.__doc__
        func = functools.update_wrapper(func2, func)
        if hasattr(host, func.__name__):
            oldfunc = getattr(host,func.__name__)
            if hasattr(oldfunc,"__orig_func__"):
                warnings.warn (f"Replacing patched '{func.__name__}' in '{host}' by new patch.")
                func.__orig_func__ = oldfunc.__orig_func__
            else:
                func.__orig_func__ = oldfunc
            func.__host__ = host
            func._restore_orig = types.MethodType (restore, func)
            realname = f"patch version: from {func}.\n{func.__name__}._restore_orig () restores the original version."
            setattr(host,func.__name__,functools.wraps(func.__orig_func__)(func))
            if isinstance(func.__doc__, str):
                func.__doc__ = func.__doc__ + "\n" + realname
            # --- now find direct imports of orig_func and replace by wrapper
            @functools.wraps(func)
            def wrap_dynamic(*args):
                return getattr(host, func.__name__)(*args)
            if hasattr(wrap_dynamic,'_restore_orig'):
                del wrap_dynamic._restore_orig
            for m in sys.modules.values ():
                try:
                    for nm, val in m.__dict__.items ():
                        if val is func.__orig_func__:
                            print (f"Replacing direct import {m.__name__}.{nm} by dynamic wrapper")
                            setattr(m, nm, wrap_dynamic)
                except TypeError:
                    pass
        else:
            warnings.warn (f"Added function {func.__name__} to {host} rather than patching existing one")
            setattr(host,func.__name__, func)
        return func

    return install_patch
